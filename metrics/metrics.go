package metrics

import (
	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	//cpuTemp = prometheus.NewGauge(prometheus.GaugeOpts{
	//	Name: "cpu_temperature_celsius",
	//	Help: "Current temperature of the CPU.",
	//})
	StatusRequesterCounter = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Name: "status_requester_counter",
			Help: "This is status requester counter",
		})
	StatusDurationCounter = prometheus.NewHistogram(
		prometheus.HistogramOpts{
			Name:    "status_duration_counter",
			Help:    "This is status duration counter",
			Buckets: []float64{10, 100, 500, 1000},
		})
	MoveRequesterCounter = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Name: "move_requester_counter",
			Help: "This is move requester counter",
		})
	MoveDurationCounter = prometheus.NewHistogram(
		prometheus.HistogramOpts{
			Name:    "move_duration_counter",
			Help:    "This is move duration counter",
			Buckets: []float64{10, 100, 500, 1000},
		})
	// Courier Repo metrics
	CourierRepoSaveCounter = prometheus.NewCounter(
		prometheus.CounterOpts{
			Name: "courier_repo_save_counter",
			Help: "This is courier repo method 'Save' counter",
		})
	CourierRepoSaveDurationCounter = prometheus.NewHistogram(
		prometheus.HistogramOpts{
			Name:    "courier_repo_save_duration_counter",
			Help:    "This is courier repo method 'Save' duration counter",
			Buckets: []float64{10, 100, 500, 1000},
		})
	CourierRepoGetOneCounter = prometheus.NewCounter(
		prometheus.CounterOpts{
			Name: "courier_repo_get_one_counter",
			Help: "This is courier repo method 'GetOne' counter",
		})
	CourierRepoGetOneDurationCounter = prometheus.NewHistogram(
		prometheus.HistogramOpts{
			Name:    "courier_repo_get_one_duration_counter",
			Help:    "This is courier repo method 'GetOne' duration counter",
			Buckets: []float64{10, 100, 500, 1000},
		})
	// Order Repo metrics
	OrderRepoSaveCounter = prometheus.NewCounter(
		prometheus.CounterOpts{
			Name: "order_repo_save_counter",
			Help: "This is order repo method 'Save' counter",
		})
	OrderRepoSaveDurationCounter = prometheus.NewHistogram(
		prometheus.HistogramOpts{
			Name:    "order_repo_save_duration_counter",
			Help:    "This is order repo method 'Save' duration counter",
			Buckets: []float64{10, 100, 500, 1000},
		})
	OrderRepoGetByIDCounter = prometheus.NewCounter(
		prometheus.CounterOpts{
			Name: "order_repo_getbyid_counter",
			Help: "This is order repo method 'GetById' counter",
		})
	OrderRepoGetByIDDurationCounter = prometheus.NewHistogram(
		prometheus.HistogramOpts{
			Name:    "order_repo_getbyid_duration_counter",
			Help:    "This is order repo method 'GetByID' duration counter",
			Buckets: []float64{10, 100, 500, 1000},
		})
	OrderRepoGetCountCounter = prometheus.NewCounter(
		prometheus.CounterOpts{
			Name: "order_repo_getcount_counter",
			Help: "This is order repo method 'GetCount' counter",
		})
	OrderRepoGetCountDurationCounter = prometheus.NewHistogram(
		prometheus.HistogramOpts{
			Name:    "order_repo_getcount_duration_counter",
			Help:    "This is order repo method 'GetCount' duration counter",
			Buckets: []float64{10, 100, 500, 1000},
		})
	OrderRepoRemoveCounter = prometheus.NewCounter(
		prometheus.CounterOpts{
			Name: "order_repo_remove_counter",
			Help: "This is order repo method 'RemoveOldOrders' counter",
		})
	OrderRepoRemoveDurationCounter = prometheus.NewHistogram(
		prometheus.HistogramOpts{
			Name:    "order_repo_remove_duration_counter",
			Help:    "This is order repo method 'RemoveOldOrders' duration counter",
			Buckets: []float64{10, 100, 500, 1000},
		})
	OrderRepoGetByRadiusCounter = prometheus.NewCounter(
		prometheus.CounterOpts{
			Name: "order_repo_getbyradius_counter",
			Help: "This is order repo method 'GetByRadius' counter",
		})
	OrderRepoGetByRadiusDurationCounter = prometheus.NewHistogram(
		prometheus.HistogramOpts{
			Name:    "order_repo_getbyradius_duration_counter",
			Help:    "This is order repo method 'GetByRadius' duration counter",
			Buckets: []float64{10, 100, 500, 1000},
		})
)

func init() {
	//prometheus.MustRegister(cpuTemp)
	prometheus.MustRegister(
		OrderRepoSaveCounter,
		OrderRepoSaveDurationCounter,
		OrderRepoRemoveCounter,
		OrderRepoRemoveDurationCounter,
		OrderRepoGetByIDCounter,
		OrderRepoGetByIDDurationCounter,
		OrderRepoGetCountCounter,
		OrderRepoGetCountDurationCounter,
		OrderRepoGetByRadiusCounter,
		OrderRepoGetByRadiusDurationCounter,

		CourierRepoSaveCounter,
		CourierRepoSaveDurationCounter,
		CourierRepoGetOneCounter,
		CourierRepoGetOneDurationCounter,
		StatusRequesterCounter,
		StatusDurationCounter,
		MoveRequesterCounter,
		MoveDurationCounter)
}
func PrometheusHandler() gin.HandlerFunc {
	h := promhttp.Handler()
	return func(c *gin.Context) {
		h.ServeHTTP(c.Writer, c.Request)
	}
}
