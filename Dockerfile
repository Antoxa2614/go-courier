FROM golang:1.20.4-alpine3.15 AS builder
COPY geotask /go/src/gitlab.com/antoxa2614/go-courier/
WORKDIR /go/src/gitlab.com/antoxa2614/go-courier/

# Create slimest possible image
RUN go build -ldflags="-w -s" -o /go/bin/server /go/src/gitlab.com/antoxa2614/go-courier/cmd/api


FROM alpine:3.15
# Copy binary from builder
COPY --from=builder /go/bin/server /go/bin/server
COPY public /app/public
COPY .env /app/.env

WORKDIR /app
# Set entrypoint
ENTRYPOINT ["/go/bin/server"]