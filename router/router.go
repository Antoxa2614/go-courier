package router

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/antoxa2614/go-courier/metrics"
	"gitlab.com/antoxa2614/go-courier/module/courierfacade/controller"
)

type Router struct {
	courier *controller.CourierController
}

func NewRouter(courier *controller.CourierController) *Router {
	return &Router{courier: courier}
}

func (r *Router) CourierAPI(router *gin.RouterGroup) {
	// прописать роуты для courier API
	router.GET("/status", r.courier.GetStatus)
	router.GET("/ws", r.courier.Websocket)

}

func (r *Router) Swagger(router *gin.RouterGroup) {
	router.GET("/swagger", swaggerUI)
}

func (r *Router) Prometheus(router *gin.RouterGroup) {
	router.GET("/metrics", metrics.PrometheusHandler())
}
