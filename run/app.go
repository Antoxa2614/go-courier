package run

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/antoxa2614/go-courier/geo"
	"gitlab.com/antoxa2614/go-courier/internal/db"
	cservice "gitlab.com/antoxa2614/go-courier/module/courier/service"
	cstorage "gitlab.com/antoxa2614/go-courier/module/courier/storage"
	"gitlab.com/antoxa2614/go-courier/module/courierfacade/controller"
	cfservice "gitlab.com/antoxa2614/go-courier/module/courierfacade/service"
	oservice "gitlab.com/antoxa2614/go-courier/module/order/service"
	ostorage "gitlab.com/antoxa2614/go-courier/module/order/storage"
	"gitlab.com/antoxa2614/go-courier/router"
	"gitlab.com/antoxa2614/go-courier/server"
	"gitlab.com/antoxa2614/go-courier/workers/order"
	"net/http"
	"os"
)

type App struct {
}

func NewApp() *App {
	return &App{}
}

func (a *App) Run() error {

	dbx := db.NewSqlDB()
	defer dbx.Close()

	// Чтобы все отчищало в редис
	// инициализация разрешенной зоны
	allowedZone := geo.NewAllowedZone()
	// инициализация запрещенных зон
	disAllowedZones := []geo.PolygonChecker{geo.NewDisAllowedZone1(), geo.NewDisAllowedZone2()}

	// инициализация хранилища заказов
	orderStorage := ostorage.NewOrderStorage(dbx)
	// инициализация сервиса заказов
	orderService := oservice.NewOrderService(orderStorage, allowedZone, disAllowedZones)

	orderGenerator := order.NewOrderGenerator(orderService)
	orderGenerator.Run()

	oldOrderCleaner := order.NewOrderCleaner(orderService)
	oldOrderCleaner.Run()

	// инициализация хранилища курьеров
	courierStorage := cstorage.NewCourierStorage(dbx)
	// инициализация сервиса курьеров
	courierSevice := cservice.NewCourierService(courierStorage, allowedZone, disAllowedZones)

	// инициализация фасада сервиса курьеров
	courierFacade := cfservice.NewCourierFacade(courierSevice, orderService)

	// инициализация контроллера курьеров
	courierController := controller.NewCourierController(courierFacade)

	// инициализация роутера
	routes := router.NewRouter(courierController)
	// инициализация сервера
	r := server.NewHTTPServer()
	// инициализация группы роутов
	api := r.Group("/api")
	// инициализация роутов
	routes.CourierAPI(api)

	mainRoute := r.Group("/")

	routes.Swagger(mainRoute)
	routes.Prometheus(mainRoute)
	// инициализация статических файлов
	r.NoRoute(gin.WrapH(http.FileServer(http.Dir("public"))))

	// запуск сервера
	//serverPort := os.Getenv("SERVER_PORT")

	if os.Getenv("ENV") == "prod" {
		certFile := "/app/certs/cert.pem"
		keyFile := "/app/certs/private.pem"
		return r.RunTLS(":443", certFile, keyFile)
	}

	return r.Run()
}
