package models

import (
	cm "gitlab.com/antoxa2614/go-courier/module/courier/models"
	om "gitlab.com/antoxa2614/go-courier/module/order/models"
)

type CourierStatus struct {
	Courier cm.Courier `json:"courier"`
	Orders  []om.Order `json:"orders"`
}
