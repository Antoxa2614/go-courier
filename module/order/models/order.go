package models

import "time"

type Order struct {
	ID            int64     `json:"id" db:"id"`
	Price         float64   `json:"price"db:"price"`
	DeliveryPrice float64   `json:"delivery_price" db:"delivery_price"`
	Lng           float64   `json:"lng" db:"lng"`
	Lat           float64   `json:"lat" db:"lat"`
	IsDelivered   bool      `json:"is_delivered" db:"is_delivered"`
	CreatedAt     time.Time `json:"created_at" db:"created_at"`
}
