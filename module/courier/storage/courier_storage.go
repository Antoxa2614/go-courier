package storage

import (
	"context"
	"github.com/jmoiron/sqlx"
	"gitlab.com/antoxa2614/go-courier/metrics"
	"gitlab.com/antoxa2614/go-courier/module/courier/models"
	"log"
	"time"
)

//go:generate go run github.com/vektra/mockery/v2@v2.20.0 --name CourierStorager
type CourierStorager interface {
	Save(ctx context.Context, courier models.Courier) error // сохранить курьера по ключу courier
	GetOne(ctx context.Context) (*models.Courier, error)    // получить курьера по ключу courier
}

type CourierStorage struct {
	db *sqlx.DB
}

func (c *CourierStorage) Save(ctx context.Context, courier models.Courier) error {
	start := time.Now()
	metrics.CourierRepoSaveCounter.Inc()
	defer func() {
		//metrics.StatusRequesterCounter.Dec()
		metrics.CourierRepoSaveDurationCounter.Observe(float64(time.Since(start).Milliseconds()))
	}()

	query := `INSERT INTO couriers VALUES ($1, $2, $3, $4) ON CONFLICT (id) DO UPDATE SET score = $2, lat = $3, lng = $4`

	courierId := 0

	_, err := c.db.Exec(query, courierId, courier.Score, courier.Location.Lat, courier.Location.Lng)
	if err != nil {
		return err
	}

	return nil

}

func (c *CourierStorage) GetOne(ctx context.Context) (*models.Courier, error) {
	start := time.Now()
	metrics.CourierRepoGetOneCounter.Inc()
	defer func() {
		metrics.CourierRepoGetOneDurationCounter.Observe(float64(time.Since(start).Milliseconds()))
	}()

	res := &models.Courier{}

	query := `SELECT id, score, lat as "location.lat", lng as "location.lng" FROM couriers LIMIT 1`

	err := c.db.Get(res, query)
	if err != nil {
		return nil, err
	}

	return res, err
}

func NewCourierStorage(db *sqlx.DB) CourierStorager {
	query := `CREATE TABLE IF NOT EXISTS couriers (
    id INT,
    score INT,
    lng FLOAT,
    lat FLOAT,
    CONSTRAINT uc_couriers_id UNIQUE (id)); `
	_, err := db.Exec(query)
	if err != nil {
		log.Fatal(err)
	}
	return &CourierStorage{db: db}
}
